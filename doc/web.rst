.. _web:

On the web
==========

Some experimental splats can also be heard on `SoundCloud
<http://soundcloud.com/verdigrix>`_.

You can find some splats as well as other music, software and electronics
things on `verdigris.org <http://verdigris.org>`_.

Please make your creations or reactions be heard by sending them to
`info@verdigris.org <mailto:info@verdigris.org>`_.
