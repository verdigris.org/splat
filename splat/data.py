# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Splat - splat/data.py
#
# Copyright (C) 2012-2015 Guillaume Tucker

"""Splat data management"""

import hashlib
import os
import wave

try:
    import cPickle as pickle
except ImportError:
    import pickle

try:
    # For extra standard audio formats
    import audiotools
    HAS_AUDIOTOOLS = True
    from io import StringIO
except ImportError:
    HAS_AUDIOTOOLS = False

import _splat
import splat

# Used in Splat Audio Fragment files (.saf)
SAF_MAGIC = 'Splat!'
SAF_FORMAT_FLAT = 1
SAF_FORMAT_MMAP = 2


# -----------------------------------------------------------------------------
# Utilities

def _get_fmt(f, fmt):
    if fmt is None:
        if isinstance(f, str):
            fmt = f.rpartition(os.extsep)[2].lower()
        else:
            raise ValueError("Format required with file objects")
    return fmt


def _read_chunks(frag, read_frames, frame_size, sample_type):
    rem = len(frag)
    cur = 0
    chunk_size = int(65536 / frame_size)  # read in blocks of 64K

    while rem > 0:
        n = min(chunk_size, rem)
        data = bytearray(read_frames(n))
        frag.import_bytes(data, frag.rate, frag.channels, sample_type, cur)
        rem -= n
        cur += n


# -----------------------------------------------------------------------------
# Audio file openers

def open_wav(wav_file, fmt=None):
    """WAV file opener"""
    if _get_fmt(wav_file, fmt) != 'wav':
        return None

    w = wave.open(wav_file, 'rb')
    channels = w.getnchannels()
    n_frames = w.getnframes()
    rate = w.getframerate()
    sample_width_bytes = w.getsampwidth()
    frame_size = sample_width_bytes * channels
    s_width = sample_width_bytes * 8
    frag = Fragment(channels, rate, length=n_frames)
    _read_chunks(frag, w.readframes, frame_size, f'int{s_width:d}')
    w.close()
    return frag


def open_saf(saf_file, fmt=None):
    """Splat Audio Format file opener"""
    if _get_fmt(saf_file, fmt) != 'saf':
        return None

    # pylint: disable=unused-argument
    # pylint: disable=too-many-arguments
    def open_saf_flat(f, attr, rate, channels, length, precision, **kwargs):
        frag = Fragment(rate=rate, channels=channels, length=length)
        frame_size = int(channels * precision / 8)
        _read_chunks(frag, (lambda x: f.read(x * frame_size)), frame_size,
                     sample_type=f'float{precision:d}')
        return frag

    # pylint: disable=unused-argument
    def open_saf_mmap(f, attr, rate, channels, length, **kwargs):
        info = pickle.load(f)
        rel = os.path.dirname(f.name)
        mmap_names = (i['mmap'] for i in info)
        mmap_paths = list(os.path.join(rel, m) for m in mmap_names)
        return Fragment(rate=rate, channels=channels, length=length,
                        mmap=mmap_paths)

    is_str = isinstance(saf_file, str)
    with (open(saf_file, 'rb') if is_str else saf_file) as saf:
        if saf.readline().decode().strip('\n') != SAF_MAGIC:
            return None

        attr_str = saf.readline().decode().strip('\n')
        attr = dict((kv.split('=') for kv in attr_str.split(' ')))
        opts = ['rate', 'channels', 'length', 'precision', 'format']
        kwargs = dict(zip(opts, (int(attr[x]) for x in opts)))
        saf_openers = {
            SAF_FORMAT_FLAT: open_saf_flat,
            SAF_FORMAT_MMAP: open_saf_mmap,
        }

        try:
            format_name = kwargs.pop('format')
            saf_opener = saf_openers[format_name]
        except KeyError as exc:
            raise ValueError(
                f"Format not supported in this version of Splat: {format_name}"
            ) from exc
        frag = saf_opener(saf, attr, **kwargs)
        if frag.md5() != attr['md5']:
            raise ValueError("Fragment MD5 sum mismatch")
        return frag


audio_file_openers = [open_wav, open_saf]

if HAS_AUDIOTOOLS is True:
    def open_audiotools(f_name, **_):
        """audiotools multi-format file opener"""
        if not isinstance(f_name, str):
            return None
        try:
            f = audiotools.open(f_name)
        except audiotools.UnsupportedFile:
            return None
        sample_width = f.bits_per_sample()
        pcm = f.to_pcm()
        frag = Fragment(rate=f.sample_rate(), channels=f.channels(),
                        length=f.total_frames())
        frame_size = frag.channels * sample_width / 8
        chunk_size = 65536 / frame_size
        rem = f.total_frames()
        cur = 0
        while rem > 0:
            frames = pcm.read(chunk_size)
            if frames.frames == 0:
                # Some lossy formats like MP3 have slightly varying data length
                break
            raw_bytes = bytearray(frames.to_bytes(False, True))
            frag.import_bytes(raw_bytes, frag.rate, frag.channels,
                              f'int{sample_width:d}', cur)
            rem -= frames.frames
            cur += frames.frames
        return frag
    audio_file_openers.append(open_audiotools)


# -----------------------------------------------------------------------------
# Audio file savers

def save_wav(wav_file, frag, start, end, sample_type='int16'):
    """Save audio data as WAV"""
    sample_width = splat.sample_types[sample_type]
    w = wave.open(wav_file, 'wb')
    w.setnchannels(frag.channels)
    w.setsampwidth(int(sample_width / 8))
    w.setframerate(frag.rate)
    w.setnframes(len(frag))
    raw_bytes = frag.export_bytes(sample_type, start, end)
    w.writeframes(bytearray(raw_bytes))
    w.close()


def save_saf(saf_file, frag, start, end):
    """Save audio data as Splat Audio Format"""
    is_str = isinstance(saf_file, str)

    def write_saf_header(f, attrs):
        hdr = ' '.join('='.join(str(x) for x in kv) for kv in attrs.items())
        f.write(f'{SAF_MAGIC}\n{hdr}\n'.encode('utf-8'))

    def save_saf_flat(f, frag, attrs):
        raw_bytes = frag.export_bytes(start=start, end=end)
        frame_size = frag.channels * splat.SAMPLE_WIDTH / 8
        length = int(len(raw_bytes) / frame_size)
        md5sum = hashlib.md5(raw_bytes).hexdigest()
        attrs.update({
            'format': SAF_FORMAT_FLAT,
            'md5': md5sum,
            'length': length,
        })
        write_saf_header(f, attrs)
        f.write(raw_bytes)

    def save_saf_mmap(f, frag, attrs):
        attrs.update({
            'format': SAF_FORMAT_MMAP,
            'md5': frag.md5(),
            'length': len(frag),
        })
        write_saf_header(f, attrs)
        info = frag.info
        for chan in info:
            chan['mmap'] = os.path.basename(chan['mmap'])
        pickle.dump(info, f)

    chan_info = frag.info
    if chan_info[0]['mmap_temp'] is False:
        saf_saver = save_saf_mmap
    else:
        saf_saver = save_saf_flat

    for c in chan_info[1:]:
        if (c['mmap'] is None and chan_info[0]['mmap'] is not None) or \
           (c['mmap'] is not None and chan_info[0]['mmap'] is None) or \
           (c['mmap_temp'] != chan_info[0]['mmap_temp']):
            raise ValueError("Inconsistent use of mmap across channels")

    attrs = {
        'version': splat.VERSION_STR,
        'build': splat.BUILD,
        'channels': frag.channels,
        'rate': frag.rate,
        'precision': splat.SAMPLE_WIDTH,
    }

    # Don't close the file if it was provided as an open file object
    # pylint: disable=consider-using-with
    saf = open(saf_file, 'wb') if is_str else saf_file
    saf_saver(saf, frag, attrs)
    if is_str:
        saf.close()


audio_file_savers = {'wav': save_wav, 'saf': save_saf}

if HAS_AUDIOTOOLS is True:
    channel_masks = [0x4, 0x3, 0x7, 0x33, 0x37, 0x137, 0x637, 0x737, 0xF37,
                     0x7F7, 0xFF7, 0x2FF7, 0x6FF7, 0x7FF7, 0x17FF7, 0x2FFF7]

    # This whole thing should be replaced with data backends
    # pylint: disable=too-many-arguments
    def _save(cls, fname, frag, start, end, sample_type='int16',
              channel_mask=None, compression=None):
        if not isinstance(fname, str):
            raise TypeError('File name needed when saving with audiotools')
        if channel_mask is None:
            channel_mask = channel_masks[frag.channels]
        data = frag.export_bytes(sample_type, start, end)
        str_io = StringIO(str(data))
        sample_width = splat.sample_types[sample_type]
        reader = audiotools.PCMReader(str_io, frag.rate, frag.channels,
                                      channel_mask, sample_width)
        cls.from_pcm(fname, reader, compression, len(frag))

    audio_fmt_cls = {
        'ogg': audiotools.VorbisAudio,
        'flac': audiotools.FlacAudio,
        'mp3': audiotools.MP3Audio,
    }
    for audio_fmt, audio_cls in audio_fmt_cls.items():
        # pylint: disable=cell-var-from-loop
        audio_file_savers[audio_fmt] = (
            lambda *args, **kwargs: _save(audio_cls, *args, **kwargs)
        )


# -----------------------------------------------------------------------------
# Data classes

class Fragment(_splat.Fragment):
    """A fragment of sound data

    Create an empty sound fragment with the given number of ``channels``,
    sample ``rate`` in Hertz and initial ``duration`` in seconds or ``length``
    in number of samples per channel.  The default number of channels is 2, the
    minimum is 1 and the maximum is currently fixed at 16.  The default sample
    rate is 48000.  If no duration is specified, the fragment will be empty.
    Otherwise, the samples are initialised to 0 (silence).

    An optional ``name`` can be given to the fragment to identify it later in
    application code.  This is also used in :py:class:`splat.seq.SampleSet` to
    refer to named samples.  It can be accessed later with
    :py:attr:`splat.data.Fragment.name`.

    The fragment can also be allocated using ``mmap``.  If set to ``True``, the
    fragment will be backed with a temporary mmap file.  It can also be a
    string used as a path prefix to create persistent mmap files, or a list of
    paths with existing files to reuse.  In this case, there must be the same
    number of paths as of ``channels`` and the ``length`` should be set to
    match the mmap file sizes as new fragments are always resized to the given
    length.

    All Splat sound data is contained in :py:class:`splat.data.Fragment`
    objects.  They are accessible as a mutable sequence of tuples of floating
    point values to represent the samples across all the audio channels.  The
    length of each sample tuple is equal to the number of channels of the
    fragment.

    .. note::

       It is rather slow to access and modify all the samples of a Fragment
       using the standard Python sequence interface.  The underlying
       implementation in C can handle all common data manipulations much
       faster using the Fragment methods documented below.
    """

    @classmethod
    def open(cls, in_file, fmt=None, name=None):
        """Open a file to create a sound fragment by importing audio data.

        Open a sound file specified by ``in_file``, which can be either a file
        name or a file-like object, and import its contents into a new
        :py:class:`splat.data.Fragment` instance, which is then returned.

        The format can be specified via the ``fmt`` argument, otherwise it may
        be automatically detected whenever possible.  Only a limited set of
        :ref:`audio_files` formats are supported (currently only ``wav`` and
        ``saf``).  All the samples are converted to floating point values with
        the native precision of the fragment.
        """
        fmt = _get_fmt(in_file, fmt)
        for opener in audio_file_openers:
            frag = opener(in_file, fmt)
            if frag is None:
                continue
            if name is not None:
                frag.name = name
            elif isinstance(in_file, str):
                frag.name = os.path.splitext(os.path.basename(in_file))[0]
            return frag
        raise ValueError("Unsupported file format")

    # normalize should be dropped as it's implicitly changing the data
    # pylint: disable=too-many-arguments
    def save(self, out_file, fmt=None, start=None, end=None,
             normalize=True, **kwargs):
        """Save the contents of the audio fragment into a file.

        If ``out_file`` is a string, a file is created with this name;
        otherwise, it must be a file-like object.  The contents of the audio
        fragment are written to this file.  It is possible to save only a part
        of the fragment using the ``start`` and ``end`` arguments with times in
        seconds.  The contents of the fragment will be automatically normalized
        unless ``normalized`` is set to ``False``.

        The ``fmt`` argument is a string to identify the output file format to
        use.  If ``None``, the file name extension is used.  It may otherwise
        be a file-like object, in which case the format needs to be specified.
        Extra arguments that are specific to the file format may be added.

        The ``wav`` format accepts an extra ``sample_type`` argument to specify
        the sample type, which also defines the number of bytes per sample.
        The default is ``int16`` (16 bits).
        """
        fmt = _get_fmt(out_file, fmt)
        saver = audio_file_savers.get(fmt, None)
        if saver is None:
            raise ValueError(f"Unsupported file format: {fmt}")
        if normalize is True:
            self.normalize()
        saver(out_file, self, start, end, **kwargs)

    def dup(self):
        """Duplicate this fragment into a new one and return it."""
        dup_frag = Fragment(channels=self.channels, rate=self.rate)
        dup_frag.mix(self)
        return dup_frag

    def grow(self, duration=None, length=None):
        """Resize if ``duration`` or ``length`` is greater than current."""
        if duration is not None:
            if duration > self.duration:
                self.resize(duration=duration)
        elif length is not None:
            if length > len(self):
                self.resize(length=length)
        else:
            raise ValueError("neither new duration nor length was supplied")

    def md5(self, sample_type=splat.SAMPLE_TYPE, as_md5_obj=False):
        """Get the MD5 checksum of this fragment's data.

        The data is first converted to samples as specified by ``sample_type``
        and ``sample_width`` in bytes.  Then the MD5 checksum is returned as a
        string unless ``as_md5_obj`` is set to True in which case an ``md5``
        object is returned instead."""
        md5sum = hashlib.md5(self.export_bytes(sample_type))
        return md5sum if as_md5_obj is True else md5sum.hexdigest()

    def n2s(self, n):
        """Convert a sample index number ``n`` into a time in seconds."""
        return float(n) / self.rate

    def s2n(self, s):
        """Convert a time in seconds ``s`` into a sample index number."""
        return int(s * self.rate)
