# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Splat - splat/sources.py
#
# Copyright (C) 2012-2013 Guillaume Tucker

"""Splat signal sources"""

# pylint: disable=unused-import
from _splat import sine, square, triangle, overtones
